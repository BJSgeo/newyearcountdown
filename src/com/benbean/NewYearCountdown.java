package com.benbean;

/*
 * Program to count down to the 2020 New Year!
 * 1-8-2019
 * Copyright (c) 2019 Ben Goldberg
 * Have fun!!!
 * Note: In Eastern Standard Time
 */

import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.Instant;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.swing.*;
import javax.swing.border.*;

public class NewYearCountdown {
	final static double NewYear = 1577854800;
	static JTextField daysVal, hoursVal, minutesVal, secondsVal, happy, newyear;
	static JLabel firework;

	private static void addLabel(JTextField t, int pos, Font f, JFrame fr) {
		t.setFont(f);
        t.setEditable(false);
        t.setBounds(pos, 80, 100, 20);
        t.setBorder(new EmptyBorder(0, 0, 0, 0));
        t.setOpaque(false);
        fr.add(t);
	}

	private static void addLabel(JTextField t, int posX, int posY, Font f, JFrame fr, int width) {
		t.setFont(f);
        t.setEditable(false);
        t.setBounds(posX, posY, 100, width);
        t.setBorder(new EmptyBorder(0, 0, 0, 0));
        t.setOpaque(false);
        fr.add(t);
	}

	public static void main(String[] args) throws FontFormatException, IOException {
		Font font = new Font("Courier New", Font.BOLD, 72);
		Font fontTwo = new Font("Bahnschrift", Font.BOLD, 20);
		Font comfortaa = Font.createFont(Font.TRUETYPE_FONT, NewYearCountdown.class.getResourceAsStream("/com/benbean/Comfortaa-Bold.ttf"));
		comfortaa = comfortaa.deriveFont(54.0f);
        JFrame frame = new JFrame("Countdown to the New Year!");
        JTextField days = new JTextField("DAYS");
        daysVal = new JTextField("-");
        JTextField hours = new JTextField("HOURS");
        hoursVal = new JTextField("-");
        JTextField minutes = new JTextField("MINUTES");
        minutesVal = new JTextField("-");
        JTextField seconds = new JTextField("SECONDS");
        secondsVal = new JTextField("-");
        URL url = NewYearCountdown.class.getResource("/com/benbean/fireworks.gif");
        ImageIcon fireworks = new ImageIcon(url);
        firework = new JLabel(fireworks);
        firework.setBounds(0, 100, 500, 474);
        firework.setVisible(false);
        happy = new JTextField("HAPPY");
        happy.setBounds(10, 250, 480, 54);
        happy.setFont(comfortaa);
        happy.setBorder(new EmptyBorder(0, 0, 0, 0));
        happy.setEditable(false);
        happy.setOpaque(false);
        happy.setHorizontalAlignment(JTextField.CENTER);
        happy.setForeground(new Color(255, 255, 255));
        happy.setVisible(false);
        newyear = new JTextField("NEW YEAR!!");
        newyear.setBounds(10, 350, 480, 54);
        newyear.setFont(comfortaa);
        newyear.setBorder(new EmptyBorder(0, 0, 0, 0));
        newyear.setEditable(false);
        newyear.setOpaque(false);
        newyear.setHorizontalAlignment(JTextField.CENTER);
        newyear.setForeground(new Color(255, 255, 255));
        newyear.setVisible(false);
        frame.add(happy);
        frame.add(newyear);
        frame.add(firework);
        addLabel(days, 10, fontTwo, frame);
        addLabel(hours, 175, fontTwo, frame);
        addLabel(minutes, 375, fontTwo, frame);
        addLabel(seconds, 575, fontTwo, frame);
        addLabel(daysVal, 0, 0, font, frame, 80);
        addLabel(hoursVal, 185, 0, font, frame, 80);
        addLabel(minutesVal, 385, 0, font, frame, 80);
        addLabel(secondsVal, 585, 0, font, frame, 80);
        frame.add(days);
        frame.setLayout(null);
        frame.setSize(700, 600);
        frame.setResizable(false);
        frame.setVisible(true);
		final ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
		executor.scheduleAtFixedRate(NewYearCountdown::update, 0, 100, TimeUnit.MILLISECONDS);
	}

	private static void update() {
		double time = Instant.now().getEpochSecond();
		int d = (int)((NewYear-time)/86400.0);
		int h = (int)(((NewYear-time)%86400.0)/3600.0);
		int m = (int)(((NewYear-time)%86400.0)%3600.0/60.0);
		int s = (int)((((NewYear-time)%86400.0)%3600.0)%60.0);
		if (d + h + m + s < 0) {
			d = 0;
			h = 0;
			m = 0;
			s = 0;
		}
		daysVal.setText(Integer.toString(d));
		hoursVal.setText(Integer.toString(h));
		minutesVal.setText(Integer.toString(m));
		secondsVal.setText(Integer.toString(s));
		if (NewYear-time <= 0) {
			firework.setVisible(true);
			happy.setVisible(true);
			newyear.setVisible(true);
		}
	}

}
